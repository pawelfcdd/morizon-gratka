<?php

namespace App\Repository;

use App\Entity\District;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method District|null find($id, $lockMode = null, $lockVersion = null)
 * @method District|null findOneBy(array $criteria, array $orderBy = null)
 * @method District[]    findAll()
 * @method District[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistrictRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, District::class);
    }

    /**
     * @param string $city
     * @param array $queryParameters
     * @return int|mixed|string
     */
    public function getFilteredResults(string $city, array $queryParameters)
    {
        $query = $this->createQueryBuilder('d')
            ->where('d.square BETWEEN :minSquare AND :maxSquare')
            ->andWhere('d.population BETWEEN :minPopulation AND :maxPopulation')
            ->andWhere('d.city = :city')
            ->setParameters([
                'city' => $city,
                'minSquare' => floatval($queryParameters['squareMin']),
                'maxSquare' => floatval($queryParameters['squareMax']),
                'minPopulation' => intval($queryParameters['populationMin']),
                'maxPopulation' => intval($queryParameters['populationMax']),
            ])
            ->getQuery();

        return $query->getResult();
    }


    /**
     * @param string $city
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMinSquareValue(string $city)
    {
        $query = $this->createQueryBuilder('d')
            ->select('MIN(d.square)')
            ->where('d.city = :city')
            ->setParameters([
                'city' => $city,
            ])
            ->getQuery();

        return floatval($query->getSingleResult()[1]);
    }

    /**
     * @param string $city
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMaxSquareValue(string $city)
    {
        $query = $this->createQueryBuilder('d')
            ->select('MAX(d.square)')
            ->where('d.city = :city')
            ->setParameters([
                'city' => $city,
            ])
            ->getQuery();

        return floatval($query->getSingleResult()[1]);
    }

    public function getMinPopulationValue(string $city)
    {
        $query = $this->createQueryBuilder('d')
            ->select('MIN(d.population)')
            ->where('d.city = :city')
            ->setParameters([
                'city' => $city,
            ])
            ->getQuery();

        return intval($query->getSingleResult()[1]);
    }

    public function getMaxPopulationValue(string $city)
    {
        $query = $this->createQueryBuilder('d')
            ->select('MAX(d.population)')
            ->where('d.city = :city')
            ->setParameters([
                'city' => $city,
            ])
            ->getQuery();

        return intval($query->getSingleResult()[1]);
    }
}
