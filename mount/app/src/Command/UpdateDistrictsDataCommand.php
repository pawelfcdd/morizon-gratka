<?php


namespace App\Command;


use App\Entity\City;
use App\Entity\District;
use App\Service\SvgData\DistrictDto;
use App\Service\SvgData\SvgDataService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class UpdateDistrictsDataCommand extends Command
{
    use DistrictsTrait;

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var SvgDataService */
    private $svgDataService;

    /**
     * @param EntityManagerInterface $entityManager
     * @param SvgDataService $svgDataService
     */
    public function __construct(EntityManagerInterface $entityManager, SvgDataService $svgDataService)
    {
        $this->entityManager = $entityManager;
        $this->svgDataService = $svgDataService;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('districts:update');
        $this->setDescription('This command allows You to save districts data by city');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Wybierz miasto',
            $this->availableCities(),
            0
        );
        $question->setErrorMessage('Nieprawidlowa opcja');
        $city = $helper->ask($input, $output, $question);
        $this->getAndUpdateData($city);

        return 0;
    }

    private function getAndUpdateData(string $city)
    {
        $cityEntity = $this->entityManager->getRepository(City::class)->findOneBy(['name' => $city]);

        if (!$cityEntity instanceof City) {
            throw new \Exception('Can not find city');
        }

        switch ($cityEntity->getSlug()) {
            case 'gdansk':
                $districts = $this->svgDataService->getDataFromUrl($cityEntity->getBaseUrl(), $cityEntity->getSubPath());
                $this->updateData($cityEntity->getSlug(), $districts);
                break;
            case 'krakow':
                $districts = [];
                break;
            default:
                $districts = [];
                break;
        }
    }

    private function updateData(string $slug, array $districts): self
    {
        $dbDistricts = $this->entityManager->getRepository(District::class)->findBy(['city' => $slug]);

        foreach ($dbDistricts as $dbDistrict) {
            $this->entityManager->remove($dbDistrict);
        }

        $this->entityManager->flush();

        /** @var DistrictDto $districtDto */
        foreach ($districts as $districtDto) {
            $this->entityManager->persist((new District())
                ->setName($districtDto->getName())
                ->setCity($slug)
                ->setSquare($districtDto->getSquare())
                ->setPopulation($districtDto->getPopulation())
            );
        }

        $this->entityManager->flush();

        return $this;
    }
}