<?php


namespace App\Command;


use App\Entity\City;
use App\Entity\District;
use App\Service\SvgData\SvgDataService;
use App\Service\SvgData\DistrictDto;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class SaveDistrictsDataCommand extends Command
{
    use DistrictsTrait;

    /** @var SvgDataService */
    private $svgDataService;
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param SvgDataService $svgDataService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SvgDataService $svgDataService, EntityManagerInterface $entityManager)
    {
        $this->svgDataService = $svgDataService;
        $this->entityManager = $entityManager;
        parent::__construct();
    }


    public function configure()
    {
        $this->setName('districts:load');
        $this->setDescription('This command allows You to save districts data by city');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Wybierz miasto',
            $this->availableCities(),
            0
        );
        $question->setErrorMessage('Nieprawidlowa opcja');
        $city = $helper->ask($input, $output, $question);
        $this->getAndSaveData($city);

        return 0;
    }

    /**
     * @param string $city
     * @return $this
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function getAndSaveData(string $city): self
    {
        $cityEntity = $this->entityManager->getRepository(City::class)->findOneBy(['name' => $city]);

        if (!$cityEntity instanceof City) {
            throw new \Exception('Can not find city');
        }

        switch ($cityEntity->getSlug()) {
            case 'gdansk':
                $districts = $this->svgDataService->getDataFromUrl($cityEntity->getBaseUrl(), $cityEntity->getSubPath());
                $this->saveData($cityEntity->getSlug(), $districts);
                break;
            case 'krakow':
                $districts = [];
                break;
            default:
                $districts = [];
                break;
        }

        return $this;
    }

    private function saveData(string $city, array $districts): self
    {
        /** @var DistrictDto $districtDto */
        foreach ($districts as $districtDto) {
            $this->entityManager->persist((new District())
                ->setName($districtDto->getName())
                ->setPopulation($districtDto->getPopulation())
                ->setSquare($districtDto->getSquare())
                ->setCity($city)
            );
        }
        $this->entityManager->flush();

        return $this;
    }
}