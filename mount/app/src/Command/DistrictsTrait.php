<?php


namespace App\Command;


use App\Entity\City;

trait DistrictsTrait
{
    public function availableCities(): array
    {
        $cities = $this->entityManager->getRepository(City::class)->getCityNames();
        $result = [];

        foreach ($cities as $city) {
            $result[] = $city['name'];
        }
        return $result;
    }
}