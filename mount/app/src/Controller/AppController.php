<?php


namespace App\Controller;


use App\Entity\District;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    private const CITIES_LINK_SOURCES = [
        'gdansk' => [
            'name' => 'Gdańsk',
            'baseUrl' => 'https://www.gdansk.pl',
            'secondaryUrlPath' => 'dzielnice'
        ],
        'krakow' => [
            'name' => 'Kraków',
            'baseUrl' => 'https://www.gdansk.pl',
            'secondaryUrlPath' => 'dzielnice'
        ]
    ];

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var \App\Repository\DistrictRepository|\Doctrine\Persistence\ObjectRepository */
    private $districtRepository;
    /** @var AppPayload */
    private $appPayload;

    public function __construct(EntityManagerInterface $entityManager, AppPayload $appPayload)
    {
        $this->entityManager = $entityManager;
        $this->districtRepository = $this->entityManager->getRepository(District::class);
        $this->appPayload = $appPayload;
    }

    /**
     * @Route("/", name="app.index")
     */
    public function indexAction(): Response
    {
        return $this->render('index.html.twig', []);
    }

    /**
     * @Route("/city/{slug}", name="app.data_by_city")
     */
    public function showDataAction(string $slug, Request $request): Response
    {
        $this->appPayload->setCity(self::CITIES_LINK_SOURCES[$slug]['name']);

        if (!empty($request->query->all())) {
            $this->appPayload
                ->setData($this->districtRepository->getFilteredResults($slug, $request->query->all()))
                ->setSquareMin($request->query->get('squareMin'))
                ->setSquareMax($request->query->get('squareMax'))
                ->setPopulationMin($request->query->get('populationMin'))
                ->setPopulationMax($request->query->get('populationMax'));
        } else {
            $this->appPayload
                ->setData($this->districtRepository->findBy(['city' => $slug]))
                ->setSquareMin($this->districtRepository->getMinSquareValue($slug))
                ->setSquareMax($this->districtRepository->getMaxSquareValue($slug))
                ->setPopulationMin($this->districtRepository->getMinPopulationValue($slug))
                ->setPopulationMax($this->districtRepository->getMaxPopulationValue($slug));
        }

        return $this->render('list.html.twig', $this->appPayload->toArray());
    }
}