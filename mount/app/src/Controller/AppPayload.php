<?php


namespace App\Controller;


use App\Entity\District;
use Symfony\Component\Serializer\SerializerInterface;

class AppPayload
{
    /** @var SerializerInterface */
    private $serializer;

    /** @var District[] */
    private $data;
    /** @var float */
    private $squareMin;
    /** @var float */
    private $squareMax;
    /** @var int */
    private $populationMin;
    /** @var int */
    private $populationMax;
    /** @var string */
    private $city;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return District[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param District[] $data
     * @return AppPayload
     */
    public function setData(array $data): AppPayload
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return float
     */
    public function getSquareMin(): float
    {
        return $this->squareMin;
    }

    /**
     * @param float $squareMin
     * @return AppPayload
     */
    public function setSquareMin(float $squareMin): AppPayload
    {
        $this->squareMin = $squareMin;
        return $this;
    }

    /**
     * @return float
     */
    public function getSquareMax(): float
    {
        return $this->squareMax;
    }

    /**
     * @param float $squareMax
     * @return AppPayload
     */
    public function setSquareMax(float $squareMax): AppPayload
    {
        $this->squareMax = $squareMax;
        return $this;
    }

    /**
     * @return int
     */
    public function getPopulationMin(): int
    {
        return $this->populationMin;
    }

    /**
     * @param int $populationMin
     * @return AppPayload
     */
    public function setPopulationMin(int $populationMin): AppPayload
    {
        $this->populationMin = $populationMin;
        return $this;
    }

    /**
     * @return int
     */
    public function getPopulationMax(): int
    {
        return $this->populationMax;
    }

    /**
     * @param int $populationMax
     * @return AppPayload
     */
    public function setPopulationMax(int $populationMax): AppPayload
    {
        $this->populationMax = $populationMax;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return AppPayload
     */
    public function setCity(string $city): AppPayload
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return json_decode($this->serializer->serialize($this, 'json'), true);
    }
}