<?php

namespace App\DataFixtures;

use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private const CITIES_DATA = [
        [
            'name' => 'Gdańsk',
            'slug' => 'gdansk',
            'baseUrl' => 'https://www.gdansk.pl',
            'subPath' => 'dzielnice'
        ],
        [
            'name' => 'Kraków',
            'slug' => 'krakow',
            'baseUrl' => '',
            'subPath' => ''
        ]
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::CITIES_DATA as $cityData) {
            $manager->persist((new City())
                ->setName($cityData['name'])
                ->setSlug($cityData['slug'])
                ->setBaseUrl($cityData['baseUrl'])
                ->setSubPath($cityData['subPath'])
            );
        }

        $manager->flush();
    }
}
