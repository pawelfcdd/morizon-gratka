<?php


namespace App\Service\SvgData;


class DistrictDtoMapper
{
    /**
     * @param array $data
     * @return DistrictDto[]
     */
    public function mapSvgData(array $data): array
    {
        $result = [];

        foreach ($data as $item) {
            $square = floatval(str_replace(',', '.', explode(' ', $item[2])[1]));
            $population = intval(explode(' ', $item[3])[2]);

            $result[] = (new DistrictDto())
                ->setName($item[1])
                ->setSquare($square)
                ->setPopulation($population)
            ;
        }

        return $result;
    }
}