<?php


namespace App\Service\SvgData;


class DistrictDto
{
    /** @var string */
    private $name;
    /** @var float */
    private $square;
    /** @var int */
    private $population;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DistrictDto
     */
    public function setName(string $name): DistrictDto
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getSquare(): float
    {
        return $this->square;
    }

    /**
     * @param float $square
     * @return DistrictDto
     */
    public function setSquare(float $square): DistrictDto
    {
        $this->square = $square;
        return $this;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }

    /**
     * @param int $population
     * @return DistrictDto
     */
    public function setPopulation(int $population): DistrictDto
    {
        $this->population = $population;
        return $this;
    }
}