<?php


namespace App\Service\SvgData;


use simplehtmldom\HtmlNode;
use simplehtmldom\HtmlWeb;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SvgDataService
{
    /** @var HttpClientInterface */
    private $httpClient;

    /**
     * @param HttpClientInterface $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $baseUrl
     * @param string $subUrl
     * @return DistrictDto[]
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getDataFromUrl(string $baseUrl, string $subUrl): array
    {
        $polygons = $this->getSvgPolygons($baseUrl . DIRECTORY_SEPARATOR . $subUrl);
        $dom = new \DOMDocument();
        $result = [];
        /** @var HtmlNode $polygon */
        foreach ($polygons as $polygon) {
            $response = $this->httpClient->request(
                'GET',
                $baseUrl . DIRECTORY_SEPARATOR . 'subpages/dzielnice/html/4-dzielnice_mapa_alert.php?id=' . $polygon->attr['id']
            );

            $i = 0;
            $dom->loadHTML($response->getContent());
            foreach($dom->getElementsByTagName('div') as $node)
            {
                $array[$i] = strip_tags($dom->saveHTML($node));
                $i++;
            }

            unset($array[0]);

            if (!in_array($array, $result)) {
                $result[] = $array;
            }
        }

        return (new DistrictDtoMapper())->mapSvgData($result);
    }

    private function getSvgPolygons(string $url)
    {
        $client = new HtmlWeb();
        $html = $client->load($url);
        /** @var HtmlNode $svg */
        $svg = $html->find('svg')[0];

        return $svg->children[2]->children[0]->children[0]->children;
    }
}