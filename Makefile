install:
	docker-compose up -d --build
	docker-compose exec app composer install
	docker-compose exec app npm install
	docker-compose exec app mv node_modules public/node_modules
	docker-compose exec app bin/console doctrine:database:create
	docker-compose exec app bin/console doctrine:migration:migrate
	docker-compose exec app bin/console doctrine:fixtures:load
load-districts:
	docker-compose exec app bin/console districts:load
update-districts:
	docker-compose exec app bin/console districts:update
down:
	docker-compose down
start:
	docker-compose up -d