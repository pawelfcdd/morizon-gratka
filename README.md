## Steps to install

1. Run `make install` to build the application

## Load districts data

1. Run `make load-districts` to load districts data for the selected city

## Update districts data

1. Run `make update-districts` to update districts data

## Test application

1. Go to `http://localhost` to view the application in browser
